/*******************************************************************************
 *  Project Title: ArduBot - Arduino robot 
 *  Author: Ing. David Hrbaty
 *  Version: 1.1
 *  Revision Date: 21/12/2014
 *
 *  This file is free software; you can redistribute it and/or modify
 *  it under the terms of either the GNU General Public License version 2
 *  or the GNU Lesser General Public License version 2.1, both as
 *  published by the Free Software Foundation.
 *
 * Schematic pinout:
 *
 * Left Servo motor:           D06
 * Right Servo motor:          D07
 * Head servo:                 D08
 * Ultrasonic sensor TRIG PIN: D09
 * Ultrasonic sensor ECHO PIN: D10
 * Speaker:                    D11
 * IR Receiver:                D12
 * 
 * Edits:
 *  - Servo movement functions edited to remove the delays
 *  - Revised and added some commentary
 *  - Revised the servo moveStop control code to actually detach the motors instead of
 *    just setting their speed to "zero"
 *******************************************************************************/

#include <Servo.h>

/* 
 * Below are the symbolic constants. 
 * Instead of having to type in a non-sensical pin number each time we want to do something 
 * we can write an easy to understand name which represents the pin, 
 * the compiler will then replace the names with the numbers
 */
#define TRIG_PIN 9    
#define ECHO_PIN 10
#define LED 13

#define SERVO_HEAD 8
#define SERVO_LEFT 6
#define SERVO_RIGHT 7

/*
 * Here we have created four 'objects', three for the servos and one for the IR receiver
 */
Servo servo_head;
Servo servo_left;
Servo servo_right;

/* 
 *  ref: https://github.com/gskielian/Robox/blob/master/Step%203%20Coding/MotorController_v_3.0.ino
 *  These are scales for the motor speeds. [0-100%]. 
 *  0 is the min speed i.e. stopped, 
 *  100 is the max speed i.e. supersonic
*/
double ForwardSpeed = 35.0;  
double RotateSpeed = 20.0;   
double BackwardSpeed = 25.0; 

/*
 * Below we are creating unsigned integer variables which we will use later on in the code. 
 * They are unsigned as they will only have postive values
*/
unsigned int duration;
unsigned int distance;
unsigned int FrontDistance;
unsigned int LeftDistance;
unsigned int RightDistance;
unsigned int LeftDiagonalDistance;
unsigned int RightDiagonalDistance;
unsigned int Time;
unsigned int adDistance;
unsigned int CollisionCounter;

char choice;

char turnDirection;  // Gets 'l', 'r' or 'f' depending on which direction is obstacle free

/*
 * Limits for obstacles
*/
const int distanceLimit = 27;           // Front distance limit in cm
const int sideDistanceLimit = 12;       // Side distance limit in cm
const int turnTime = 300;               // Time needed to turn robot

/*
 *  Various control variables
*/
int distanceCounter = 0;
int numcycles = 0;  // Number of cycles used to rotate with head during moving
int roam = 1;       // Switching between automatic and manual mode of moving
int mon = 0;        // Switching between monitor and manual mode of moving

/*
 * Sets up the initial configurations.
 * This block happens once on startup
 */
void setup()                                           
{
  Serial.begin(9600);   // Setup serial for diagnostics                          
  
  pinMode(LED, OUTPUT);             // Set a pin as OUPTUT for controlling an LED
  servo_head.attach(SERVO_HEAD);    // Attach the head servo
  servo_left.attach(SERVO_LEFT);    // Attach the left drive servo
  servo_right.attach(SERVO_RIGHT);  // Attach the right drive servo
  pinMode(TRIG_PIN, OUTPUT);        // Set the ultrasonic's TRIG pin to output
  pinMode(ECHO_PIN, INPUT);         // Set the ultrasonic's ECHO pin to input
  moveStop();                       // Tell the robot to stop
}

/*
 * The main control loop
 * This block repeats itself while the Arduino is turned on
 */
void loop()                                           
{
  // If roam is set to 1, then go into autonomous mode
  if(roam == 1)
  {
    go();
  }

  // If monitor is set to 1, then go into monitor mode
  if(mon == 1) 
  {
    monitor();
  }

}

/*
 * This function toggle between autonomous and stop mode
 */
void toggleRoam(){                                    
  if(roam == 0){
    roam = 1;
    Serial.println("Activated Roam Mode");
  }
  else{
    roam = 0;
    moveStop();
    Serial.println("De-activated Roam Mode");
  }
}

/*
 * This function toggle between monitor and stop mode
 */
void toggleMonitor(){                                  
  if(mon == 0){
    mon = 1;
    moveStop();
    Serial.println("Activated Monitor Mode");
  }
  else{
    mon = 0;
    moveStop();
    Serial.println("De-activated Monitor Mode");
  }
}

/*
 * Moves the robot forward at the specified speed
 */ 
void moveForward()
{
  // from https://github.com/gskielian/Robox/blob/master/Step%203%20Coding/MotorController_v_3.0.ino
  double speed = ForwardSpeed;

  attachWheelServos(); //in case we disconnected them

  //The CRS thinks it is always at 90 degrees
  //if you tell it to go to 180 degrees it will go really fast clock-wise
  //0 degrees really quickly counterclockwise
  // 95 really slowly clockwise
     
  double leftspeed = (speed/100.0*90.0 + 90) ;
  double rightspeed = (-speed/100.0*90.0 + 90) ;  // translates it to servo-speak (servo input is from 0-180)
  
  servo_left.write( (int) leftspeed);             
  servo_right.write((int) rightspeed); // Right wheel is backwards
}

/*
 *  Moves the robot backwards
 */
void moveBackward ()
{
  // from https://github.com/gskielian/Robox/blob/master/Step%203%20Coding/MotorController_v_3.0.ino
    double speed = BackwardSpeed; 

    attachWheelServos(); //in case we disconnected them

    double leftspeed = (-speed/100.0*90.0 + 90) ;
    double rightspeed = (speed/100.0*90.0 + 90) ;  // translates it to servo-speak (servo input is from 0-180)
    
    servo_left.write( (int) leftspeed);             
    servo_right.write((int) rightspeed);

}

/*
 *  Rotates the robot left
 */
void moveLeft ()
{
  // from https://github.com/gskielian/Robox/blob/master/Step%203%20Coding/MotorController_v_3.0.ino
    double speed = RotateSpeed;

    attachWheelServos(); //in case we disconnected them

    double leftspeed = (speed/100.0*90.0 + 90) ;  // translates it to servo-speak (servo input is from 0-180)
    double rightspeed = (speed/100.0*90.0 + 90) ;  // translates it to servo-speak (servo input is from 0-180)
    
    servo_left.write((int) leftspeed);             
    servo_right.write((int) rightspeed);
}

/*
 * Rotates the robot right
 */
void moveRight ()
{
    // from https://github.com/gskielian/Robox/blob/master/Step%203%20Coding/MotorController_v_3.0.ino
    double speed = RotateSpeed;
    
    attachWheelServos(); //in case we disconnected them
     
    double leftspeed =  (-speed/100.0*90.0 + 90) ;  // translates it to servo-speak (servo input is from 0-180)
    double rightspeed =  (-speed/100.0*90.0 + 90) ;  // translates it to servo-speak (servo input is from 0-180)
    
    servo_left.write((int) leftspeed);             
    servo_right.write((int) rightspeed); 
}

/*
 * Stop the robot moving by disabling servos
 */
void moveStop()                                     //This function tells the robot to stop moving
{
  Serial.println("");
  Serial.println("Stopping");

  servo_left.detach();
  servo_right.detach();
}

/*
 * Attaches both the drive servos
 */
void attachWheelServos()
{
   servo_left.attach(SERVO_LEFT);
   servo_right.attach(SERVO_RIGHT);
}

/*
 * This function determines the distance things are away from the ultrasonic sensor
 */
int scan()                                         
{
  long pulse;
  Serial.println("Scanning distance");
  digitalWrite(TRIG_PIN,LOW);
  delayMicroseconds(5);                                                                              
  digitalWrite(TRIG_PIN,HIGH);
  delayMicroseconds(15);
  digitalWrite(TRIG_PIN,LOW);
  pulse = pulseIn(ECHO_PIN,HIGH);
  distance = round( pulse*0.01657 );
  Serial.println(distance);
}

/*
 * Measures distances to the 
 *    right, left, front, left diagonal, right diagonal 
 * and assign them in cm to the variables 
 *    rightscanval, leftscanval, centerscanval, ldiagonalscanval and rdiagonalscanval 
 * (there are 5 points for distance testing)
 */
void watchsurrounding()
{ 
  scan();
  FrontDistance = distance;
  Serial.println("Front distance measuring done");
  if(FrontDistance < distanceLimit) 
  {
    moveStop;
  }
  servo_head.write(130);
  delay(100);
  scan();
  LeftDiagonalDistance = distance;
  Serial.println("Left diagonal distance measuring done");
  if(LeftDiagonalDistance < distanceLimit)
  {
    moveStop();
  }
  servo_head.write(162);
  delay(300);
  scan();
  LeftDistance = distance;
  Serial.println("Left distance measuring done");
  if(LeftDistance < sideDistanceLimit)
  {
    moveStop();
  }
  servo_head.write(130);
  delay(100);
  scan();
  LeftDiagonalDistance = distance;
  Serial.println("Left diagonal distance measuring done");
  if(LeftDiagonalDistance < distanceLimit)
  {
    moveStop();
  }
  servo_head.write(98);
  delay(100);
  scan();
  FrontDistance = distance;
  Serial.println("Front distance measuring done");
  if(FrontDistance < distanceLimit)
  {
    moveStop();
  }
  servo_head.write(66);
  delay(100);
  scan();
  RightDiagonalDistance = distance;
  Serial.println("Right diagonal distance measuring done");
  if(RightDiagonalDistance < distanceLimit)
  {
    moveStop();
  }
  servo_head.write(34);
  delay(100);
  scan();
  RightDistance = distance;
  Serial.println("Right distance measuring done");
  if(RightDistance < sideDistanceLimit)
  {
    moveStop();
  }
 
  servo_head.write(98); //Finish looking around (look forward again)
  delay(300);
  Serial.println("Measuring done");
}

/*
 * Decide the right way without obstacles
 */
char decide()
{
  watchsurrounding();
  if ( LeftDistance < sideDistanceLimit && RightDistance < sideDistanceLimit && FrontDistance < distanceLimit ) {
    Serial.println("Choice result is: BACK"); 
    choice = 'b';
  }
  else if (LeftDistance > RightDistance && LeftDistance > FrontDistance){
    Serial.println("Choise result is: LEFT");
    choice = 'l';
  }
  else if (RightDistance > LeftDistance && RightDistance > FrontDistance){
    Serial.println("Choise result is: RIGHT");
    choice = 'r';
  }
  else{
    Serial.println("Choise result is: FORWARD");
    choice = 'f';
  }
  return choice;
}

/*
 * The main movement logic for autonomous mode
 */
void go() 
{
  moveForward();
  ++numcycles;
  if(numcycles>40)  // After 40 cycles of code measure surrounding obstacles
  {
    Serial.println("Front obstancle detected");
    watchsurrounding();
    if( LeftDistance < sideDistanceLimit || LeftDiagonalDistance < sideDistanceLimit)
    {
      Serial.println("Moving: RIGHT");
      moveRight();
      delay(turnTime);
    }
    if( RightDistance < sideDistanceLimit || RightDiagonalDistance < sideDistanceLimit)
    {
      Serial.println("Moving: LEFT");
      moveLeft();
      delay(turnTime);
    }
    numcycles=0; //Restart count of cycles
  }
  scan();
  if( distance < distanceLimit)
  {
    distanceCounter++;
  }
  if( distance > distanceLimit)
  {
    distanceCounter = 0;
  }
  if(distanceCounter > 7)    // robot reachaed 7 times distance limit in front of the robot, so robot must stop immediately and decide right way
  {
    moveStop();
    turnDirection = decide();
     switch (turnDirection){
      case 'l':
        moveLeft();
        delay(turnTime);
        break;
      case 'r':
        moveRight();
        delay(turnTime);
        break;
      case 'b':
        moveBackward();
        delay(2*turnTime);
        break;
      case 'f':
        break;
    }
    distanceCounter = 0;
  }
}

/*
 * Monitoring mode (no driving, just scanning with the head)
 */
void monitor() 
{
    servo_head.write(98);
    delay(1000);    
    scan();    
    if(distance < distanceLimit)
    {
      servo_head.write(138);           //Move the servo to the left (my little servos didn't like going to 180 so I played around with the value until it worked nicely)
      delay(300);
      Serial.println(distance);
      servo_head.write(58);
      delay(300);
      servo_head.write(138);
      delay(300);
      servo_head.write(58);
      delay(300);
    }
    
    
}
